---
layout: single
sitemap: true
permalink: /about/
title: About
---
I'm something of a productivity and self improvement nerd (in addition to just being a general sci-fi/fantasy nerd). I played D&D in High School, collected comic books and hung out with the rest of the people in my school who were interested in that kind of stuff. I ran track and cross country as well, but my real interest was just in reading books and imagining new worlds with my friends.

I was so into the nerd culture that my friends and I designed comic books and other science fiction stories which still bounce around in my brain, looking for a way to get out. [I still work on some of those ideas](/author) and I split my time between reading science-fiction/fantasy books and reading self-help/productivity/business books.

Just to give you an idea, in High School I was reading Terry Brooks, David Eddings, Stephen Covey and Stephen Hawking. And that doesn't include Stan Lee . So, ya, nerd.

Which brings me to today.

Yup. Pretty much the same books, just branching out and reading people like Dan Wells, Brandon Sanderson, Jim Butcher and then reading other things from people like Tony Robbins, Jack Canfield and Pat Flynn.

## Brief History

Dave grew up as the child of an US Air Force officer and spent the first 14 years of his life traveling from one air force base to another. Eventually he settled down in Utah after graduating from BYU with a Bachelors in Computer Science.

He spent the next 20 years working for a variety of companies within the Salt Lake and Utah counties including as a partner in [nStar Global Services][nstar]. For a full resume you can look at his [linkedin profile][linkedin].

In addition to his experience with software development, David has also been an active volunteer with the Boy Scouts of America Utah National Parks Council in a number of different positions including Council Advancement Chairman and Vice President of Program.

Together with his wife, David hosts a weekly podcast where they share tips, tricks and advice on how to raise and run an [organized family][organized_family].

Dave specializes in Software Development, Development Process, Personal and Team Leadership.

In his free time Dave plays games with his children, hikes, camps, plays soccer

## Certifications/Awards

* Silver Beaver, BSA
* [Wood Badge][woodbadge] Course Director, 2013
* Shodan, Aikido, [Aikikai Foundation][aikikai]
* Certified Scrum Master (License #: 358020), [Scrum Alliance][scrum_master]
* Certified Delphi 1.0 and 2.0 Instructor & Developer (1997-1999), [Borland][delphi]
* Certified Advanced Open Water Diver, [PADI][padi]
* [Microsoft Certified Instructor][mci] (1997-2000)

[organized_family]: http://www.organizedfamily.co
[nstar]: http://www.nstarglobal.com
[linkedin]: https://www.linkedin.com/in/digitalbias
[aikikai]: http://www.aikikai.or.jp/eng/index.html
[woodbadge]: https://en.wikipedia.org/wiki/Wood_Badge
[scrum_master]: https://www.scrumalliance.org/
[delphi]: https://groups.google.com/forum/#!topic/comp.lang.pascal.delphi.misc/mu5f2W4QSxI
[mci]: https://www.microsoft.com/en-us/learning/mct-certification.aspx
[padi]: https://www.padi.com/scuba-diving/padi-courses/course-catalog/advanced-open-water-diver-course/
